/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plataformavalidezpredictiva.clases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import plataformavalidezpredictiva.BD;

/**
 *
 * @author dynelight
 */
public class Bitacora {

    private String url;
    private String usuario;
    private String password;
    
    private BD bd;

    public Bitacora() {
        
        bd = new BD();
                        
        this.url = this.bd.get_url();                
        this.usuario = this.bd.get_usario();
        this.password = this.bd.get_password();
    }
    
    public boolean insertar_evento(int id_usuario, String evento) {
            
        boolean res = false;
        try {

            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(url, usuario, password);
            Statement stmt = conn.createStatement();

            System.out.println("INSERT INTO V_IIP_BITACORA(ID_usuario, descripcion) VALUES ('" + id_usuario + "','" + evento + ")");
            int rs = stmt.executeUpdate("INSERT INTO V_IIP_BITACORA(ID_usuario, descripcion) VALUES ('" + id_usuario + "','" + evento + "')");
            conn.close();
            res = true;

        } catch (Exception e) {
            System.out.println(e);
            res = false;

        }
        return res;
    }
    
    public String[] getBitacora() {
        String res = "";
        try {
            /**
             * Conector base de datos *
             */
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(url, usuario, password);
            Statement stmt = conn.createStatement();

            /**
             * Query para llenar Combobox de carreras *
             */
            ResultSet rs = stmt.executeQuery("SELECT ID_evento, ID_usuario, descripcion FROM V_IIP_BITACORA");

            while (rs.next()) {
                /**
                 * Concatena en un String el resultado *
                 */
                res += rs.getInt("ID_evento") + " - " + rs.getString("ID_usuario") + " - " + rs.getString("descripcion") + "\n";

            }
            rs.close();
            conn.close();

            /**
             * Quita la coma final *
             */
            res = res.substring(0, res.length());

        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
        /**
         * Devuelve como un array *
         */
        
        System.out.println("Aqui aqui 2: ");
        //System.out.println(res);
        return res.split("\n");
    }
    
}
