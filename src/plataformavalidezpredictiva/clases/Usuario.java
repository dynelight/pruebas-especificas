/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plataformavalidezpredictiva.clases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import plataformavalidezpredictiva.BD;

    
    
/**
 *
 * @author dynelight
 */
public class Usuario {
        
    private String url;
    private String usuario;
    private String password;

    private BD bd;
    
    public Usuario() {
        bd = new BD();
                        
        this.url = this.bd.get_url();                
        this.usuario = this.bd.get_usario();
        this.password = this.bd.get_password(); 
    }

    public String[] getUsuarios() {
        String res = "";
        try {
            /**
             * Conector base de datos *
             */
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(url, usuario, password);
            Statement stmt = conn.createStatement();

            /**
             * Query para llenar Combobox de carreras *
             */
            ResultSet rs = stmt.executeQuery("SELECT id, usuario, nombre, apellido_1, apellido_2 FROM V_IIP_USUARIOS ORDER BY id ");

            while (rs.next()) {
                /**
                 * Concatena en un String el resultado *
                 */
                res += rs.getInt("id") + "- " + rs.getString("usuario") + "\n";

            }
            rs.close();
            conn.close();

            /**
             * Quita la coma final *
             */
            res = res.substring(0, res.length());

        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
        /**
         * Devuelve como un array *
         */
        
        System.out.println("Aqui aqui: ");
        System.out.println(res);
        return res.split("\n");
    }

    public Map<String, String> getUsuario(int id_usuario) {
            
        Map<String, String> map = new HashMap<String, String>();


        String res = "";
        try {
            /**
             * Conector base de datos *
             */
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(url, usuario, password);
            Statement stmt = conn.createStatement();

            /**
             * Query para llenar Combobox de carreras *
             */
            ResultSet rs = stmt.executeQuery("SELECT id, usuario, nombre, apellido_1, apellido_2, correo FROM V_IIP_USUARIOS WHERE id = " + id_usuario);

            while (rs.next()) {
                /**
                 * Concatena en un String el resultado *
                 */
                res += rs.getInt("id") + " - " + rs.getString("usuario") + "\n";
                map.put("id", Integer.toString(rs.getInt("id")));
                map.put("usuario", rs.getString("usuario"));
                map.put("nombre", rs.getString("nombre"));
                map.put("apellido_1", rs.getString("apellido_1"));
                map.put("apellido_2", rs.getString("apellido_2"));
                map.put("e-mail", rs.getString("correo"));                

            }
            rs.close();
            conn.close();

            /**
             * Quita la coma final *
             */
            res = res.substring(0, res.length());

        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
        /**
         * Devuelve como un array *
         */
        
        System.out.println("Aqui aqui: ");
        System.out.println(res);
        return map;
    }
        
    public boolean crear_usuario(String login, String nombre, String apellido1, String apellido2, String email, String password_u) {
            
        boolean res = false;
        try {

            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(url, usuario, password);
            Statement stmt = conn.createStatement();

            //System.out.println("INSERT INTO V_IIP_USUARIOS(usuario, password) VALUES ('" + login + "', MD5('" + password_u + "'))");
            System.out.println("INSERT INTO V_IIP_USUARIOS(usuario, nombre, apellido_1, apellido_2, correo, password) VALUES ('" + login + "','" + apellido1 + "','" + apellido2 + "','" + nombre + "','" + email + " ',  MD5('" + password_u + "'))");
            int rs = stmt.executeUpdate("INSERT INTO V_IIP_USUARIOS(usuario, nombre, apellido_1, apellido_2, correo, password) VALUES ('" + login + "','" + apellido1 + "','" + apellido2 + "','" + nombre + "','" + email + " ', MD5('" + password_u + "'))");
            conn.close();
            res = true;

        } catch (Exception e) {
            System.out.println(e);
            res = false;

        }
        return res;
    }

    public boolean modificar_usuario(int id_usuario, String nombre, String apellido1, String apellido2, String email) {
        boolean ret = true;         
        try {

            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(url, usuario, password);
            Statement stmt = conn.createStatement();

            int rs = stmt.executeUpdate("UPDATE V_IIP_USUARIOS SET nombre='" + nombre + "', apellido_1 = '" + apellido1 + "', apellido_2 = '" + apellido2 + "', correo = '" + email + "' WHERE id = " + id_usuario);
            conn.close();
            
        } catch (Exception e) {
            
            System.out.println(e);
            ret = false;
        }
        return ret; 
    }

    
    public void borrar_usuario(int id_usuario) {
        try {

            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(url, usuario, password);
            Statement stmt = conn.createStatement();

            int rs = stmt.executeUpdate("DELETE FROM V_IIP_USUARIOS WHERE id = " + id_usuario);
            conn.close();

        } catch (Exception e) {
            System.out.println(e);

        }
    }
            
    /**public String[] getCarreras(String pareo) {
        String res = "";
        System.out.println("Res");
        try {
            /**
             * Conector base de datos *
             */
            /** Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(url, usuario, password);
            Statement stmt = conn.createStatement();

            /**
             * Query para llenar Combobox de carreras *
             */
             /**System.out.println("SELECT carrera, descripcion_carrera FROM V_IIP_CARRERA WHERE descripcion_carrera LIKE '%" + pareo + "%' OR carrera LIKE '" + pareo + "' ORDER BY carrera, descripcion_carrera");
//            ResultSet rs = stmt.executeQuery("SELECT carrera, descripcion_carrera FROM V_IIP_CARRERA ORDER BY carrera WHERE carrera LIKE '%" + pareo + "%' OR descripcion_carrera LIKE '%" + pareo + "%'");
            ResultSet rs = stmt.executeQuery("SELECT carrera, descripcion_carrera FROM V_IIP_CARRERA WHERE descripcion_carrera LIKE '%" + pareo + "%' OR carrera LIKE '" + pareo + "' ORDER BY carrera, descripcion_carrera");


            while (rs.next()) {
                /**
                 * Concatena en un String el resultado *
                 */
              /**  res += rs.getInt("carrera") + " - " + rs.getString("descripcion_carrera") + "\n";**/

            /** }
            rs.close();
            conn.close();

            /**
             * Quita la coma final *
             */
            /** res = res.substring(0, res.length() - 1);

        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
            System.out.println(e.getMessage());
        }
        /**
         * Devuelve como un array *
         */
        /** return res.split("\n");
    }    **/
    
    
    
}
