/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plataformavalidezpredictiva;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.ArrayList;

/**
 *
 * @author Dyne
 */
public class BD {

    private String url;
    private String usuario;
    private String password;
    
    private ArrayList<String> carnes_finales;
    
    private SortedMap<String, String> estudiantesAtributosMapa;
    private SortedMap<String, SortedMap<String, String>> estudiantesCursosMapa;
    private SortedMap<String, SortedMap<String, String>> estudiantesPAAMapa;
    
    public ArrayList<String> get_carnes_finales(){
        return this.carnes_finales;
    }
    
    public ArrayList<String> get_carnes_iniciales(String params){
        ArrayList<String> carnes_iniciales = new ArrayList();
        
        System.out.println("Se cae aqui?");
        try {
            
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(url, this.usuario, this.password);
            Statement stmt = conn.createStatement();
            
            System.out.println("SELECT DISTINCT V_IIP_INFORMACION_ESTUDIANTE.carne FROM V_IIP_INFORMACION_ESTUDIANTE, V_IIP_CARRERA_ESTUDIANTE, V_IIP_ESTADS_RESULT_PAA_HIS WHERE 1=1 " + params);

            ResultSet rs = stmt.executeQuery("SELECT DISTINCT V_IIP_INFORMACION_ESTUDIANTE.carne FROM V_IIP_INFORMACION_ESTUDIANTE, V_IIP_CARRERA_ESTUDIANTE, V_IIP_ESTADS_RESULT_PAA_HIS WHERE 1=1 AND " + params);

            while (rs.next()) {
                carnes_iniciales.add(rs.getString("carne"));
            }
             rs.close();
            conn.close();            
        } catch (Exception e) {
            System.out.println(e);

        }
        System.out.println("Sip");
        
        return carnes_iniciales;

        
        
    }

    
    
    public BD() {

        /**File file = new File("config.dat");
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String text = null;
            String[] aux = null;

            while ((text = reader.readLine()) != null) {
                aux = text.split("=");
                if (aux[0].equals("url")) {
                    this.url = aux[1];
                }
                if (aux[0].equals("usuario")) {
                    this.usuario = aux[1];
                }
                if (aux[0].equals("password")) {
                    this.password = aux[1];
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
            }
        }**/
        
        /**this.url = "jdbc:mysql://192.168.1.50:3306/pruebasespecificas";
        this.usuario = "pe";
        this.password = "password";**/
        
        this.carnes_finales = null;
        
        

        this.url = "jdbc:mysql://163.178.102.202:3306/pruebasespecificas";
        this.usuario = "MariaPaula1";
        this.password = "V1ll4r34l.!";

    }
    
    public String get_usario(){
        return this.usuario;
    }

    public String get_password(){
        return this.password;
    }

    public String get_url(){
        return this.url;
    }

    public boolean cambiar_contrasena(int login, String u_password){
        System.out.println("Foo");
        boolean res = false;
        try {

            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(url, usuario, password);
            Statement stmt = conn.createStatement();

            int rs = stmt.executeUpdate("UPDATE V_IIP_USUARIOS SET password=MD5('" + u_password + "') WHERE id=" + login);
            conn.close();
            res = true;

        } catch (Exception e) {
            System.out.println(e);
            res = false;

        }
        return res;
    }
    
    public int login(String usuario, String password) {
        int id_usuario = -1;
        Boolean auth = false;
        String db_password = "";
        String password_md5 = "";


        // Conector base de datos

        try {

            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(url, this.usuario, this.password);
            Statement stmt = conn.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT password FROM V_IIP_USUARIOS where usuario = '" + usuario + "'");

            while (rs.next()) {
                db_password = rs.getString("password");
            }

            rs = stmt.executeQuery("SELECT MD5('" + password + "');");
            System.out.println("SELECT MD5('" + password + "')");

            while (rs.next()) {
                password_md5 = rs.getString("MD5('" + password + "')");
            }

            if (password_md5.equals(db_password)) {
                rs = stmt.executeQuery("SELECT id FROM V_IIP_USUARIOS where usuario = '" + usuario + "'");

                while (rs.next()) {
                    id_usuario = rs.getInt("id");
                }
            }
            
            rs.close();
            conn.close();            
        } catch (Exception e) {
            System.out.println(e);

        }

        return id_usuario;
    }

    public boolean testConn() {
        boolean funciono = false;
        String msg = "";
        try {
            /**
             * Conector base de datos *
             */
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(url, usuario, password);
            funciono = true;
            conn.close();
        } catch (Exception e) {
            msg = e.toString();
            funciono = false;
        }
        return funciono;
    }

    public String[] getCarreras() {
        String res = "";
        try {
            /**
             * Conector base de datos *
             */
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(url, usuario, password);
            Statement stmt = conn.createStatement();

            /**
             * Query para llenar Combobox de carreras *
             */
            ResultSet rs = stmt.executeQuery("SELECT carrera, descripcion_carrera FROM V_IIP_CARRERA ORDER BY carrera ");

            while (rs.next()) {
                /**
                 * Concatena en un String el resultado *
                 */
                res += rs.getInt("carrera") + " - " + rs.getString("descripcion_carrera") + "\n";

            }
            rs.close();
            conn.close();

            /**
             * Quita la coma final *
             */
            res = res.substring(0, res.length() - 1);

        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
        /**
         * Devuelve como un array *
         */
        return res.split("\n");
    }

    public String[] getCarreras(String pareo) {
        String res = "";
        System.out.println("Res");
        try {
            /**
             * Conector base de datos *
             */
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(url, usuario, password);
            Statement stmt = conn.createStatement();

            /**
             * Query para llenar Combobox de carreras *
             */
            System.out.println("SELECT carrera, descripcion_carrera FROM V_IIP_CARRERA WHERE descripcion_carrera LIKE '%" + pareo + "%' OR carrera LIKE '" + pareo + "' ORDER BY carrera, descripcion_carrera");
//            ResultSet rs = stmt.executeQuery("SELECT carrera, descripcion_carrera FROM V_IIP_CARRERA ORDER BY carrera WHERE carrera LIKE '%" + pareo + "%' OR descripcion_carrera LIKE '%" + pareo + "%'");
            ResultSet rs = stmt.executeQuery("SELECT carrera, descripcion_carrera FROM V_IIP_CARRERA WHERE descripcion_carrera LIKE '%" + pareo + "%' OR carrera LIKE '" + pareo + "' ORDER BY carrera, descripcion_carrera");


            while (rs.next()) {
                /**
                 * Concatena en un String el resultado *
                 */
                res += rs.getInt("carrera") + " - " + rs.getString("descripcion_carrera") + "\n";

            }
            rs.close();
            conn.close();

            /**
             * Quita la coma final *
             */
            res = res.substring(0, res.length() - 1);

        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
            System.out.println(e.getMessage());
        }
        /**
         * Devuelve como un array *
         */
        return res.split("\n");
    }

    public String determinarCriterio(String[] params) {
        String criterio = "";
        
        for (int i = 0; i < params.length; i++) {
            if (params[i] != null) {
                switch (i) {
                    case 0:
                        String carreraAux[] = params[0].split("-");
                        criterio += " AND V_IIP_CARRERA_ESTUDIANTE.carrera = " + carreraAux[0] + " ";
                        break;

                    case 1:
                        //Año Ingreso
                        
                        String carne = "";

                        criterio += " AND V_IIP_INFORMACION_ESTUDIANTE.carne LIKE '";
                        int ano = Integer.parseInt(params[1]);
                        if (ano < 2000) {
                            criterio += params[1].charAt(2);
                            criterio += params[1].charAt(3);
                        } else {
                            if ((ano >= 2000) && (ano < 2010)) {
                                criterio += "a";
                                criterio += params[1].charAt(3);
                            } else {
                                criterio += "b";
                                criterio += params[1].charAt(3);
                            }
                        }
                        criterio += "%' ";                        
                        break;
                    case 2:
                        // Stream to read file
                        FileInputStream archivo;

                        try {
                            // Open an input stream
                            String linea = "";
                            String lista_carnes = "";
                            archivo = new FileInputStream(params[2]);
                            BufferedReader br = new BufferedReader(new InputStreamReader(archivo));
                            System.out.println("Entro y archivo: " + params[2]);
                            while ((linea = br.readLine()) != null) {
                                lista_carnes += linea;
                                lista_carnes += ",";
                            }
                            lista_carnes = lista_carnes.substring(0, lista_carnes.length() - 1);
                            //String lista_carnes = new DataInputStream(fin).readLine();

                            // criterio += " AND V_IIP_INFORMACION_ESTUDIANTE.carne = '" + params[2] + "'";
                            criterio += " AND V_IIP_INFORMACION_ESTUDIANTE.carne IN ( ";
                            String[] arrCrit = lista_carnes.split(",");
                            for (int kk = 0; kk < arrCrit.length; kk++) {
                                criterio += "'" + arrCrit[kk] + "'";
                                if (kk != arrCrit.length - 1) {
                                    criterio += ",";
                                }
                            }
                            criterio += ") ";
                            System.out.println(criterio);
                        } catch (Exception e) {
                            System.out.println("Problemas");
                            
                            System.err.println(e);
                        }
                        break;
                    case 3:
                        criterio += " AND V_IIP_ESTADS_RESULT_PAA_HIS.anno_proceso = " + params[3] + "";
                        //Año Prueba
                        break;

                    case 4:
                        criterio += " AND V_IIP_INFORMACION_ESTUDIANTE.carne IN('" + params[4] + "')";
                        break;

                    default:

                        break;
                }

            }

        }

        System.out.println("Criterio en Funcion determinar: " + criterio);
        return criterio;
    }

    public String getCSV(String query, String queryPAA, String[] listaNotas, String filtro, String[] cursos, Boolean usarTablaNotas, Boolean usarTablaPAA, Boolean usarTablaEstudiantesCarreras) {

        String encabezadoAtributos = "";
        String encabezadoNotas = "";
        String encabezadoPAA = "";

        Connection conn;
        Statement stmt;
        ResultSet rs;
        ResultSetMetaData rsMetaData;
        int numberOfColumns;

        /**
         * Estructuras de datos para almacenar los cursos y desplegarlos en R^3*
         */
        this.estudiantesAtributosMapa = new TreeMap();
        this.estudiantesCursosMapa = new TreeMap();
        for (Map.Entry<String, SortedMap<String, String>> entry : estudiantesCursosMapa.entrySet()) {
            SortedMap<String, String> tempTree = new TreeMap();
            entry.setValue(tempTree);
        }
       this.estudiantesPAAMapa = new TreeMap();
        for (Map.Entry<String, SortedMap<String, String>> entry : estudiantesPAAMapa.entrySet()) {
            SortedMap<String, String> tempTree = new TreeMap();
            entry.setValue(tempTree);
        }
        
        System.out.println("!");

        /**
         * Obtener notas *
         */
        String res = "";
        try {

            /**
             * Conector base de datos *
             */
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, usuario, password);
            stmt = conn.createStatement();

            String auxString = "";

            /**
             * String de consulta que filtra los cursos *
             */
            if (!(cursos[0].equals(""))) {
                auxString = "AND (";
                for (int i = 0; i < listaNotas.length; i++) {
                    auxString += "V_IIP_NOTAS_ESTUDIANTE.sigla = '" + listaNotas[i] + "' ";
                    if (i != listaNotas.length - 1) {
                        auxString += "OR ";
                    }
                }
                auxString += ") ";

            }

            System.out.println("!!");
            
            //TODO: FIX
            String listaEstudiantes = "";
            if (!usarTablaPAA) {
                if(!usarTablaEstudiantesCarreras){
                    listaEstudiantes = "SELECT DISTINCT V_IIP_INFORMACION_ESTUDIANTE.carne FROM V_IIP_NOTAS_ESTUDIANTE, V_IIP_INFORMACION_ESTUDIANTE WHERE V_IIP_NOTAS_ESTUDIANTE.carne = V_IIP_INFORMACION_ESTUDIANTE.carne " + auxString + filtro + "";
                    System.out.println("Entro bien 1");
                }
                else {
                 listaEstudiantes = "SELECT DISTINCT V_IIP_CARRERA_ESTUDIANTE.carne FROM V_IIP_NOTAS_ESTUDIANTE, V_IIP_INFORMACION_ESTUDIANTE, V_IIP_CARRERA_ESTUDIANTE WHERE V_IIP_CARRERA_ESTUDIANTE.carne = V_IIP_NOTAS_ESTUDIANTE.carne AND V_IIP_INFORMACION_ESTUDIANTE.carne = V_IIP_CARRERA_ESTUDIANTE.carne " + auxString + filtro + "";
                }
            } else {
                if(!usarTablaEstudiantesCarreras){
                    listaEstudiantes = "SELECT DISTINCT V_IIP_INFORMACION_ESTUDIANTE.carne FROM V_IIP_NOTAS_ESTUDIANTE, V_IIP_INFORMACION_ESTUDIANTE, V_IIP_ESTADS_RESULT_PAA_HIS WHERE V_IIP_ESTADS_RESULT_PAA_HIS.identificacion_estudiante = V_IIP_INFORMACION_ESTUDIANTE.identificacion " + auxString + filtro + "";
                    System.out.println("Entro bien 2");
                }
                else {
                    listaEstudiantes = "SELECT DISTINCT V_IIP_CARRERA_ESTUDIANTE.carne FROM V_IIP_NOTAS_ESTUDIANTE, V_IIP_INFORMACION_ESTUDIANTE, V_IIP_CARRERA_ESTUDIANTE, V_IIP_ESTADS_RESULT_PAA_HIS WHERE V_IIP_CARRERA_ESTUDIANTE.carne = V_IIP_NOTAS_ESTUDIANTE.carne AND  V_IIP_ESTADS_RESULT_PAA_HIS.identificacion_estudiante = V_IIP_INFORMACION_ESTUDIANTE.identificacion AND V_IIP_INFORMACION_ESTUDIANTE.carne = V_IIP_CARRERA_ESTUDIANTE.carne " + auxString + filtro + "";                
                }
            }
            System.out.println("!!");
            
            System.out.println("Query listaEstudiantes: " + listaEstudiantes);
            rs = stmt.executeQuery(listaEstudiantes);
            rsMetaData = rs.getMetaData();

            /**
             * Inserci贸n de los estudiantes en el mapa *
             */
            System.out.println("!!");
            this.carnes_finales = new ArrayList();

            while (rs.next()) {
                this.carnes_finales.add((String) (rs.getObject(1)).toString());                            
                estudiantesAtributosMapa.put((String) (rs.getObject(1)).toString(), "");
                estudiantesCursosMapa.put((String) (rs.getObject(1)).toString(), new TreeMap());
                estudiantesPAAMapa.put((String) (rs.getObject(1)).toString(), new TreeMap());

            }
            System.out.println("@!!!@");
            //System.out.println("Query: " + query + " " + auxString + " " + filtro);
            System.out.println("@!!!@");
            rs = stmt.executeQuery(query + " " + auxString + " " + filtro);
            System.out.println("!!!");

            rsMetaData = rs.getMetaData();
            System.out.println("!!!");
            
            /**
             * Cantidad de Columnas *
             */
            numberOfColumns = rsMetaData.getColumnCount();
            System.out.println("!!!1");

            /**
             * Encabezado archivo .csv *
             */
            for (int i = 1; i < numberOfColumns + 1; i++) {
                encabezadoAtributos += rsMetaData.getColumnName(i);
                if (i < numberOfColumns) {
                    encabezadoAtributos += ",";
                }
            }
            String estudianteActual = "";
            System.out.println("!!!2");

            res = "";
            while (rs.next()) {
                for (int i = 1; i <= numberOfColumns; i++) {
                    if (i == 1) {
                        estudianteActual = (String) (rs.getObject(i)).toString();
                        res = "";
                    } else {
                        res += (String) (rs.getObject(i)).toString();
                        if (i != numberOfColumns) {
                            res += ",";
                        }
                    }
                }

                estudiantesAtributosMapa.put(estudianteActual, res);
                res = "";
            }

            System.out.println("!!!3");

            /**
             * Selecci贸n de Cursos *
             */
            if (usarTablaNotas) {
                String permutacionesSelect;
                if (usarTablaPAA) {
                    if(!usarTablaEstudiantesCarreras){
                        permutacionesSelect = "SELECT V_IIP_NOTAS_ESTUDIANTE.sigla, V_IIP_NOTAS_ESTUDIANTE.ano, V_IIP_NOTAS_ESTUDIANTE.periodo, V_IIP_NOTAS_ESTUDIANTE.creditos FROM V_IIP_INFORMACION_ESTUDIANTE, V_IIP_NOTAS_ESTUDIANTE, V_IIP_ESTADS_RESULT_PAA_HIS WHERE  V_IIP_INFORMACION_ESTUDIANTE.carne = V_IIP_NOTAS_ESTUDIANTE.carne AND V_IIP_ESTADS_RESULT_PAA_HIS.identificacion_estudiante = V_IIP_INFORMACION_ESTUDIANTE.identificacion AND V_IIP_INFORMACION_ESTUDIANTE.carne = V_IIP_NOTAS_ESTUDIANTE.carne " + auxString + filtro + " GROUP BY V_IIP_NOTAS_ESTUDIANTE.sigla, V_IIP_NOTAS_ESTUDIANTE.periodo, V_IIP_NOTAS_ESTUDIANTE.ano, V_IIP_NOTAS_ESTUDIANTE.creditos";
                    }
                    else {
                        permutacionesSelect = "SELECT V_IIP_NOTAS_ESTUDIANTE.sigla, V_IIP_NOTAS_ESTUDIANTE.ano, V_IIP_NOTAS_ESTUDIANTE.periodo, V_IIP_NOTAS_ESTUDIANTE.creditos FROM V_IIP_INFORMACION_ESTUDIANTE, V_IIP_NOTAS_ESTUDIANTE, V_IIP_CARRERA_ESTUDIANTE, V_IIP_ESTADS_RESULT_PAA_HIS WHERE  V_IIP_INFORMACION_ESTUDIANTE.carne = V_IIP_NOTAS_ESTUDIANTE.carne AND V_IIP_ESTADS_RESULT_PAA_HIS.identificacion_estudiante = V_IIP_INFORMACION_ESTUDIANTE.identificacion AND V_IIP_CARRERA_ESTUDIANTE.carne = V_IIP_NOTAS_ESTUDIANTE.carne " + auxString + filtro + " GROUP BY V_IIP_NOTAS_ESTUDIANTE.sigla, V_IIP_NOTAS_ESTUDIANTE.periodo, V_IIP_NOTAS_ESTUDIANTE.ano, V_IIP_NOTAS_ESTUDIANTE.creditos";

                    }
                   //String permutacionesSelect = "SELECT V_IIP_NOTAS_ESTUDIANTE.sigla, V_IIP_NOTAS_ESTUDIANTE.ano, V_IIP_NOTAS_ESTUDIANTE.periodo FROM V_IIP_INFORMACION_ESTUDIANTE, V_IIP_NOTAS_ESTUDIANTE, V_IIP_CARRERA_ESTUDIANTE WHERE  V_IIP_INFORMACION_ESTUDIANTE.carne = V_IIP_NOTAS_ESTUDIANTE.carne AND V_IIP_CARRERA_ESTUDIANTE.carne = V_IIP_NOTAS_ESTUDIANTE.carne " + auxString + filtro + " GROUP BY V_IIP_NOTAS_ESTUDIANTE.sigla, V_IIP_NOTAS_ESTUDIANTE.periodo, V_IIP_NOTAS_ESTUDIANTE.ano";
                } else {
                    if(!usarTablaEstudiantesCarreras){
                       //permutacionesSelect = "SELECT V_IIP_NOTAS_ESTUDIANTE.sigla, V_IIP_NOTAS_ESTUDIANTE.ano, V_IIP_NOTAS_ESTUDIANTE.periodo, V_IIP_NOTAS_ESTUDIANTE.creditos FROM V_IIP_INFORMACION_ESTUDIANTE, V_IIP_NOTAS_ESTUDIANTE WHERE  V_IIP_INFORMACION_ESTUDIANTE.carne = V_IIP_NOTAS_ESTUDIANTE.carne " + auxString + filtro + " GROUP BY V_IIP_NOTAS_ESTUDIANTE.sigla, V_IIP_NOTAS_ESTUDIANTE.periodo, V_IIP_NOTAS_ESTUDIANTE.ano, V_IIP_NOTAS_ESTUDIANTE.creditos";
                        permutacionesSelect = "SELECT V_IIP_NOTAS_ESTUDIANTE.sigla, V_IIP_NOTAS_ESTUDIANTE.ano, V_IIP_NOTAS_ESTUDIANTE.periodo, V_IIP_NOTAS_ESTUDIANTE.creditos FROM V_IIP_INFORMACION_ESTUDIANTE, V_IIP_NOTAS_ESTUDIANTE WHERE  V_IIP_INFORMACION_ESTUDIANTE.carne = V_IIP_NOTAS_ESTUDIANTE.carne " + auxString + filtro + " GROUP BY V_IIP_NOTAS_ESTUDIANTE.sigla, V_IIP_NOTAS_ESTUDIANTE.periodo, V_IIP_NOTAS_ESTUDIANTE.ano, V_IIP_NOTAS_ESTUDIANTE.creditos";
                    } else {
                        permutacionesSelect = "SELECT V_IIP_NOTAS_ESTUDIANTE.sigla, V_IIP_NOTAS_ESTUDIANTE.ano, V_IIP_NOTAS_ESTUDIANTE.periodo, V_IIP_NOTAS_ESTUDIANTE.creditos FROM V_IIP_INFORMACION_ESTUDIANTE, V_IIP_NOTAS_ESTUDIANTE, V_IIP_CARRERA_ESTUDIANTE WHERE  V_IIP_INFORMACION_ESTUDIANTE.carne = V_IIP_NOTAS_ESTUDIANTE.carne AND V_IIP_CARRERA_ESTUDIANTE.carne = V_IIP_NOTAS_ESTUDIANTE.carne " + auxString + filtro + " GROUP BY V_IIP_NOTAS_ESTUDIANTE.sigla, V_IIP_NOTAS_ESTUDIANTE.periodo, V_IIP_NOTAS_ESTUDIANTE.ano, V_IIP_NOTAS_ESTUDIANTE.creditos";

                    }
                    
                }
                System.out.println("!!! ANTES DEL PRINT");
                
                //System.out.println(permutacionesSelect);
                System.out.println("!!! DESPUES DEL PRINT");
                rs = stmt.executeQuery(permutacionesSelect);
                System.out.println("???");
                
                rsMetaData = rs.getMetaData();
                numberOfColumns = rsMetaData.getColumnCount();

                String temp = "";

                /**
                 * Creaci贸n de la estructura de llaves *
                 */
                System.out.println("Desp Query Notas");
                while (rs.next()) {
                    for (int i = 1; i < numberOfColumns + 1; i++) {
                        temp += (String) (rs.getObject(i)).toString();
                        if (i < numberOfColumns) {
                            temp += "-";
                        } else {
                            for (Map.Entry<String, SortedMap<String, String>> entry : estudiantesCursosMapa.entrySet()) {
                                SortedMap<String, String> tmp = estudiantesCursosMapa.get(entry.getKey());
                                tmp.put(temp, "null");
                                estudiantesCursosMapa.put(entry.getKey(), tmp);
                            }
                            temp = "";
                        }
                    }
                }
                System.out.println("Desp While...");



//               String notasSelect = "SELECT V_IIP_CARRERA_ESTUDIANTE.carne, V_IIP_NOTAS_ESTUDIANTE.sigla, V_IIP_NOTAS_ESTUDIANTE.ano, V_IIP_NOTAS_ESTUDIANTE.periodo, V_IIP_NOTAS_ESTUDIANTE.nota_ordinaria_num FROM V_IIP_NOTAS_ESTUDIANTE, V_IIP_CARRERA_ESTUDIANTE, V_IIP_INFORMACION_ESTUDIANTE, V_IIP_ESTADS_RESULT_PAA_HIS WHERE V_IIP_CARRERA_ESTUDIANTE.carne = V_IIP_NOTAS_ESTUDIANTE.carne AND V_IIP_INFORMACION_ESTUDIANTE.carne = V_IIP_NOTAS_ESTUDIANTE.carne AND V_IIP_INFORMACION_ESTUDIANTE.carne = V_IIP_CARRERA_ESTUDIANTE.carne AND V_IIP_ESTADS_RESULT_PAA_HIS.identificacion_estudiante = V_IIP_INFORMACION_ESTUDIANTE.identificacion " + auxString + filtro + "";

                String notasSelect = "";
                if (usarTablaPAA) {
                    if(!usarTablaEstudiantesCarreras){
                        notasSelect = "SELECT V_IIP_INFORMACION_ESTUDIANTE.carne, V_IIP_NOTAS_ESTUDIANTE.sigla, V_IIP_NOTAS_ESTUDIANTE.ano, V_IIP_NOTAS_ESTUDIANTE.periodo, V_IIP_NOTAS_ESTUDIANTE.creditos, V_IIP_NOTAS_ESTUDIANTE.nota_ordinaria_alf FROM V_IIP_NOTAS_ESTUDIANTE, V_IIP_INFORMACION_ESTUDIANTE, V_IIP_ESTADS_RESULT_PAA_HIS WHERE V_IIP_INFORMACION_ESTUDIANTE.carne = V_IIP_NOTAS_ESTUDIANTE.carne AND V_IIP_ESTADS_RESULT_PAA_HIS.identificacion_estudiante = V_IIP_INFORMACION_ESTUDIANTE.identificacion " + auxString + filtro + "";                        
                    }
                    else {
                        notasSelect = "SELECT V_IIP_CARRERA_ESTUDIANTE.carne, V_IIP_NOTAS_ESTUDIANTE.sigla, V_IIP_NOTAS_ESTUDIANTE.ano, V_IIP_NOTAS_ESTUDIANTE.periodo, V_IIP_NOTAS_ESTUDIANTE.creditos, V_IIP_NOTAS_ESTUDIANTE.nota_ordinaria_alf FROM V_IIP_NOTAS_ESTUDIANTE, V_IIP_CARRERA_ESTUDIANTE, V_IIP_INFORMACION_ESTUDIANTE, V_IIP_ESTADS_RESULT_PAA_HIS WHERE V_IIP_CARRERA_ESTUDIANTE.carne = V_IIP_NOTAS_ESTUDIANTE.carne AND V_IIP_INFORMACION_ESTUDIANTE.carne = V_IIP_NOTAS_ESTUDIANTE.carne AND V_IIP_INFORMACION_ESTUDIANTE.carne = V_IIP_CARRERA_ESTUDIANTE.carne AND V_IIP_ESTADS_RESULT_PAA_HIS.identificacion_estudiante = V_IIP_INFORMACION_ESTUDIANTE.identificacion " + auxString + filtro + "";
                    }
                } else {
                    if(!usarTablaEstudiantesCarreras){
                        notasSelect = "SELECT V_IIP_INFORMACION_ESTUDIANTE.carne, V_IIP_NOTAS_ESTUDIANTE.sigla, V_IIP_NOTAS_ESTUDIANTE.ano, V_IIP_NOTAS_ESTUDIANTE.periodo, V_IIP_NOTAS_ESTUDIANTE.creditos, V_IIP_NOTAS_ESTUDIANTE.nota_ordinaria_alf FROM V_IIP_NOTAS_ESTUDIANTE, V_IIP_INFORMACION_ESTUDIANTE WHERE V_IIP_INFORMACION_ESTUDIANTE.carne = V_IIP_NOTAS_ESTUDIANTE.carne " + auxString + filtro + "";
                    }
                    else {
                        notasSelect = "SELECT V_IIP_CARRERA_ESTUDIANTE.carne, V_IIP_NOTAS_ESTUDIANTE.sigla, V_IIP_NOTAS_ESTUDIANTE.ano, V_IIP_NOTAS_ESTUDIANTE.periodo, V_IIP_NOTAS_ESTUDIANTE.creditos, V_IIP_NOTAS_ESTUDIANTE.nota_ordinaria_alf FROM V_IIP_NOTAS_ESTUDIANTE, V_IIP_CARRERA_ESTUDIANTE, V_IIP_INFORMACION_ESTUDIANTE WHERE V_IIP_CARRERA_ESTUDIANTE.carne = V_IIP_NOTAS_ESTUDIANTE.carne AND V_IIP_INFORMACION_ESTUDIANTE.carne = V_IIP_NOTAS_ESTUDIANTE.carne AND V_IIP_INFORMACION_ESTUDIANTE.carne = V_IIP_CARRERA_ESTUDIANTE.carne " + auxString + filtro + "";
                                         
                    }

                }
                //System.out.println("Notas select: " + notasSelect);
                rs = stmt.executeQuery(notasSelect);
                System.out.println("After...");
                rsMetaData = rs.getMetaData();
                numberOfColumns = rsMetaData.getColumnCount();

                temp = "";
                String llaveEstudiante = "";


                /**
                 * Datos para la estructura de datos de estudiantes *
                 */
                
                System.out.println("After... 1");
                while (rs.next()) {
                    for (int i = 1; i < numberOfColumns + 1; i++) {
                        if (i == 1) {
                            llaveEstudiante = (String) (rs.getObject(i)).toString();
                            temp = "";
                        } else {
                            if (i == numberOfColumns) {
                                temp = temp.substring(0, temp.length() - 1);
                                SortedMap<String, String> tmp = estudiantesCursosMapa.get(llaveEstudiante);
                                tmp.put(temp, (String) (rs.getObject(i)).toString());
                                estudiantesCursosMapa.put(llaveEstudiante, tmp);
                            } else {
                                temp += (String) (rs.getObject(i)).toString();
                                temp += "-";
                            }
                        }
                    }
                }

                System.out.println("After... 2");

                SortedMap<String, String> tmpKeys = estudiantesCursosMapa.get(estudiantesCursosMapa.firstKey());
                for (Map.Entry<String, String> entry : tmpKeys.entrySet()) {
                    encabezadoNotas += entry.getKey();
                    encabezadoNotas += ",";
                }
                encabezadoNotas = encabezadoNotas.substring(0, encabezadoNotas.length() - 1);

            }


            /**
             * Selecci贸n de PAAs *
             */
            /**
             * Las llaves no dependen de los valores de los resultados. Dependen
             * del valor de las columnas. Es necesario sacar estos valores.*
             */
            if (usarTablaPAA) {
                String permutacionesPAASelect = "";
                if(!usarTablaEstudiantesCarreras){
                    permutacionesPAASelect = queryPAA
                        + " FROM V_IIP_INFORMACION_ESTUDIANTE, V_IIP_ESTADS_RESULT_PAA_HIS, V_IIP_NOTAS_ESTUDIANTE WHERE V_IIP_ESTADS_RESULT_PAA_HIS.identificacion_estudiante = V_IIP_INFORMACION_ESTUDIANTE.identificacion AND V_IIP_NOTAS_ESTUDIANTE.carne = V_IIP_INFORMACION_ESTUDIANTE.carne " + auxString + filtro;
                } else{
                    permutacionesPAASelect = queryPAA
                        + " FROM V_IIP_INFORMACION_ESTUDIANTE, V_IIP_ESTADS_RESULT_PAA_HIS, V_IIP_CARRERA_ESTUDIANTE, V_IIP_NOTAS_ESTUDIANTE WHERE V_IIP_INFORMACION_ESTUDIANTE.carne = V_IIP_CARRERA_ESTUDIANTE.carne AND V_IIP_ESTADS_RESULT_PAA_HIS.identificacion_estudiante = V_IIP_INFORMACION_ESTUDIANTE.identificacion AND V_IIP_NOTAS_ESTUDIANTE.carne = V_IIP_INFORMACION_ESTUDIANTE.carne " + auxString + filtro;

                }
                System.out.println("permutacionesPAASelect: " + permutacionesPAASelect);
                rs = stmt.executeQuery(permutacionesPAASelect);
                rsMetaData = rs.getMetaData();
                numberOfColumns = rsMetaData.getColumnCount();

                String temp = "";

                /**
                 * Creaci贸n de la estructura de llaves *
                 */
                System.out.println("DESPUES DE PERMUTACIONES");
                while (rs.next()) {
                    for (int i = 4; i < numberOfColumns + 1; i++) {
                        temp += (String) (rs.getObject(2)).toString();
                        temp += "-";
                        temp += (String) (rs.getObject(3)).toString();
                        temp += "-";
                        temp += (String) (rsMetaData.getColumnName(i).toString());

                        for (Map.Entry<String, SortedMap<String, String>> entry : estudiantesPAAMapa.entrySet()) {
                            SortedMap<String, String> tmp = estudiantesPAAMapa.get(entry.getKey());
                            tmp.put(temp, "null");
                            estudiantesPAAMapa.put(entry.getKey(), tmp);
                        }
                        //           System.out.println("Valor de la llave");
                        //           System.out.println(temp);
                        temp = "";
                    }
                }
                System.out.println("DESPUES DEL WHILE");
                String PAASelect = "";
                
                if(!usarTablaEstudiantesCarreras){
                
                    PAASelect = queryPAA
                            + " FROM V_IIP_INFORMACION_ESTUDIANTE, V_IIP_ESTADS_RESULT_PAA_HIS, V_IIP_NOTAS_ESTUDIANTE WHERE V_IIP_ESTADS_RESULT_PAA_HIS.identificacion_estudiante = V_IIP_INFORMACION_ESTUDIANTE.identificacion AND V_IIP_NOTAS_ESTUDIANTE.carne = V_IIP_INFORMACION_ESTUDIANTE.carne " + auxString + filtro;
                }
                else {
                    PAASelect = queryPAA
                        + " FROM V_IIP_INFORMACION_ESTUDIANTE, V_IIP_ESTADS_RESULT_PAA_HIS, V_IIP_CARRERA_ESTUDIANTE, V_IIP_NOTAS_ESTUDIANTE WHERE V_IIP_INFORMACION_ESTUDIANTE.carne = V_IIP_CARRERA_ESTUDIANTE.carne AND V_IIP_ESTADS_RESULT_PAA_HIS.identificacion_estudiante = V_IIP_INFORMACION_ESTUDIANTE.identificacion AND V_IIP_NOTAS_ESTUDIANTE.carne = V_IIP_INFORMACION_ESTUDIANTE.carne " + auxString + filtro;

                }
                System.out.println("PAASelect " + PAASelect);
                rs = stmt.executeQuery(PAASelect);
                System.out.println("OK!!");
                rsMetaData = rs.getMetaData();
                numberOfColumns = rsMetaData.getColumnCount();

                temp = "";
                String llaveEstudiante = "";


                /**
                 * Datos para la estructura de datos de notas *
                 */
                
                while (rs.next()) {
                    for (int i = 1; i < numberOfColumns + 1; i++) {
                        if (i == 1) {
                            llaveEstudiante = (String) (rs.getObject(i)).toString();
                            System.out.println("Llave: " + carnes_finales);
                            temp = "";
                        } else {
                            if (i >= 4) {
                                SortedMap<String, String> tmp = estudiantesPAAMapa.get(llaveEstudiante);
                                tmp.put(temp + rsMetaData.getColumnName(i).toString(), (String) (rs.getObject(i)).toString());
                                estudiantesPAAMapa.put(llaveEstudiante, tmp);
                            } else {
                                temp += (String) (rs.getObject(i)).toString();
                                temp += "-";
                            }
                        }
                    }
                }

                System.out.println("OK2!!");

                SortedMap<String, String> tmpKeys = estudiantesPAAMapa.get(estudiantesPAAMapa.firstKey());
                for (Map.Entry<String, String> entry : tmpKeys.entrySet()) {
                    encabezadoPAA += entry.getKey();
                    encabezadoPAA += ",";
                }
                encabezadoPAA = encabezadoPAA.substring(0, encabezadoPAA.length() - 1);
                System.out.println("OK3!!");
            }


            /**
             * 
             */
            String strAtributos = "";
            String strNotas = "";
            String strPAA = "";

            for (Map.Entry<String, String> entry : estudiantesAtributosMapa.entrySet()) {
                strAtributos += entry.getKey();
                strAtributos += ",";
                strAtributos += entry.getValue();
                strAtributos += "\n";

            }

            if (usarTablaNotas) {
                String aux = "";
                for (Map.Entry<String, SortedMap<String, String>> outer : estudiantesCursosMapa.entrySet()) {
                    SortedMap<String, String> inner = outer.getValue();
                    for (Map.Entry<String, String> tmpIt : inner.entrySet()) {
                        aux += tmpIt.getValue();
                        aux += ",";
                    }
                    aux = aux.substring(0, aux.length() - 1);
                    strNotas += aux;
                    strNotas += "\n";
                    aux = "";
                }
            }

            if (usarTablaPAA) {
                String aux = "";
                for (Map.Entry<String, SortedMap<String, String>> outer : estudiantesPAAMapa.entrySet()) {
                    SortedMap<String, String> inner = outer.getValue();
                    for (Map.Entry<String, String> tmpIt : inner.entrySet()) {
                        aux += tmpIt.getValue();
                        aux += ",";
                    }
                    aux = aux.substring(0, aux.length() - 1);
                    strPAA += aux;
                    strPAA += "\n";
                    aux = "";
                }
            }

            String[] arrAtributos = strAtributos.split("\n");
            String[] arrNotas = strNotas.split("\n");
            String[] arrPAA = strPAA.split("\n");

//            System.out.println("Encabezado Atributos: " + encabezadoAtributos);

            res += encabezadoAtributos;
            if (usarTablaNotas) {
                res += ",";
                res += encabezadoNotas;
            }
            if (usarTablaPAA) {
                res += ",";
                res += encabezadoPAA;
            }
            res += "\n";
            for (int i = 0; i < arrAtributos.length; i++) {
                res += arrAtributos[i];
                if (usarTablaNotas) {
                    res += ",";
                    res += arrNotas[i];
                }
                if (usarTablaPAA) {
                    res += ",";
                    res += arrPAA[i];
                }

                res += "\n";
            }

            rs.close();
            conn.close();
        } catch (Exception e) {
            System.err.println(e.toString() + "");
        }

//            System.out.println("EL GRAN TOTAL: \n" + res);
        return res;
    }
}