/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plataformavalidezpredictiva.usuarios.controllers;

import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import plataformavalidezpredictiva.clases.Usuario;
import plataformavalidezpredictiva.clases.Bitacora;

/**
 *
 * @author dynelight
 */
public class ModificarUsuario implements Initializable {
 
    public TextField txt_au_usuario;
    public TextField txt_au_nombre_usuario;
    public TextField txt_au_apellido1;
    public TextField txt_au_apellido2;
    public TextField txt_au_email;    
    public TextField txt_au_id;        
    public Button btn_agregar;      
    public Label lbl_au_status;
    
    public Usuario usuario;
    public Bitacora bitacora;
    
    public int id_usuario_modificar;
    public int id_usuario_actual;
    
    /**
     * Initializes the controller class.
     */
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.usuario = new Usuario();
        this.txt_au_id.setEditable(false);

    }   

    public void setUsuarioModificar(int id_usuario_actual, int id_usuario_modificar){
        this.id_usuario_actual = id_usuario_actual; 
        this.id_usuario_modificar = id_usuario_modificar;
        Map<String, String> usuario = this.usuario.getUsuario(this.id_usuario_modificar);
        
        this.txt_au_id.setText(usuario.get("id"));
        this.txt_au_usuario.setText(usuario.get("usuario"));
        this.txt_au_nombre_usuario.setText(usuario.get("nombre"));
        this.txt_au_apellido1.setText(usuario.get("apellido_1"));
        this.txt_au_apellido2.setText(usuario.get("apellido_2"));
        this.txt_au_email.setText(usuario.get("e-mail"));
    
    }
    public void handleModificarUsuario(ActionEvent event){
        if(this.txt_au_usuario.getText().equals("")){
            this.lbl_au_status.setText("Campo 'usuario' vacio");
        }
        else {
            boolean res = this.usuario.modificar_usuario(id_usuario_modificar, txt_au_nombre_usuario.getText(), txt_au_apellido1.getText(), txt_au_apellido2.getText(), txt_au_email.getText());            
            if(res){
                this.lbl_au_status.setText("Usuario Modificado exitosamente");
                bitacora = new Bitacora();
                System.out.println("Usuario actual: " + this.id_usuario_actual);
                System.out.println("Usuario modificar: " + this.id_usuario_modificar);
                bitacora.insertar_evento(this.id_usuario_actual, "Usuario " + this.id_usuario_modificar + " modificado con valores: " + txt_au_nombre_usuario.getText() +  " " + txt_au_apellido1.getText() +  " " + txt_au_apellido2.getText() +  " " + txt_au_email.getText() + " ");
                
            } else {
                this.lbl_au_status.setText("Error al insertar usuario");
            }
        }
    }    
    
    public void handleRegresarMain(ActionEvent event) throws Exception{
        Node node = (Node)event.getSource();
        Stage stage=(Stage) node.getScene().getWindow();
        
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/plataformavalidezpredictiva/usuarios/fxml/GestionUsuarios.fxml"));        
        
        Parent root = (Parent)fxmlLoader.load();          
        Scene scene = new Scene(root); 
        stage.setScene(scene);
               
        stage.show();
        
    }    
}
