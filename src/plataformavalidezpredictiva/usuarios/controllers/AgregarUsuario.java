/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plataformavalidezpredictiva.usuarios.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import plataformavalidezpredictiva.clases.Usuario;

/**
 *
 * @author dynelight
 */
public class AgregarUsuario implements Initializable {
 
    public TextField txt_au_usuario;
    public TextField txt_au_nombre_usuario;
    public TextField txt_au_apellido1;
    public TextField txt_au_apellido2;
    public TextField txt_au_email;    
    public PasswordField txt_au_password1;
    public PasswordField txt_au_password2;
    public Button btn_agregar;      
    public Label lbl_au_status;
    
    public Usuario usuario;
    
    /**
     * Initializes the controller class.
     */
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.usuario = new Usuario();

    }   
    
    public void handleAgregarUsuario(ActionEvent event){
        if(this.txt_au_usuario.getText().equals("")){
            this.lbl_au_status.setText("Campo 'usuario' vacio");
        }
        else {
            if(this.txt_au_password1.getText().equals( this.txt_au_password2.getText())){
                //TODO: HAY QUE MODIFICAR LA BASE DE DATOS PARA LOS OTROS CAMPOS!!!
                boolean res = this.usuario.crear_usuario(this.txt_au_usuario.getText(), this.txt_au_nombre_usuario.getText(), this.txt_au_apellido1.getText(), this.txt_au_apellido2.getText(), this.txt_au_email.getText(), this.txt_au_password1.getText());            
                if(res){
                    this.lbl_au_status.setText("Usuario agregado exitosamente");
                } else {
                    this.lbl_au_status.setText("Error al insertar usuario");
                }
            }
        }
    } 
    
    public void handleRegresarMain(ActionEvent event) throws Exception{
        Node node = (Node)event.getSource();
        Stage stage=(Stage) node.getScene().getWindow();
        
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/plataformavalidezpredictiva/usuarios/fxml/GestionUsuarios.fxml"));        
        
        Parent root = (Parent)fxmlLoader.load();          
        Scene scene = new Scene(root); 
        stage.setScene(scene);
               
        stage.show();
        
    }     
}
