/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plataformavalidezpredictiva.usuarios.controllers;

import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import plataformavalidezpredictiva.BD;
import plataformavalidezpredictiva.UsuariosController;
import plataformavalidezpredictiva.clases.Usuario;

/**
 * FXML Controller class
 *
 * @author Dyne
 */
public class GestionUsuarios implements Initializable {

    
    public ListView listaUsuarios;
    public Usuario usuario;
    public int id_usuario;
    
  
    
    /**
     * Initializes the controller class.
     */
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.usuario = new Usuario();        
        String[] usuarios = usuario.getUsuarios();

        List<String> values = Arrays.asList(usuarios);
        this.listaUsuarios.setItems(FXCollections.observableList(values));
    }    

   public void handleCargarUsuarios(ActionEvent event) {

        String[] usuarios = usuario.getUsuarios();

        List<String> values = Arrays.asList(usuarios);
        this.listaUsuarios.setItems(FXCollections.observableList(values));
    }
        
    public void handleCargarAgregarUsuario(ActionEvent event) throws Exception{
        Node node = (Node)event.getSource();
        Stage stage=(Stage) node.getScene().getWindow();
        
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/plataformavalidezpredictiva/usuarios/fxml/AgregarUsuario.fxml"));        
        
        Parent root = (Parent)fxmlLoader.load();          
        Scene scene = new Scene(root); 
        stage.setScene(scene);
               
        stage.show();
    }   
    
    public void handleCargarModificarUsuario(ActionEvent event) throws Exception{
        Node node = (Node)event.getSource();
        Stage stage=(Stage) node.getScene().getWindow();
        
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/plataformavalidezpredictiva/usuarios/fxml/ModificarUsuario.fxml"));        
 
        Parent root = (Parent)fxmlLoader.load();          
        
        ModificarUsuario controller = fxmlLoader.<ModificarUsuario>getController();
        
        String id_usuario_modificar_arr = ((String)this.listaUsuarios.getSelectionModel().getSelectedItem());
        
        String[] id_usuario_modificar_str = new String[0];
        id_usuario_modificar_str = id_usuario_modificar_arr.split("-");
        
        controller.setUsuarioModificar(this.id_usuario, Integer.parseInt(id_usuario_modificar_str[0]));
        
        Scene scene = new Scene(root); 
        stage.setScene(scene);
               
        stage.show();
    }       

        public void handleBorrarUsuario(ActionEvent event) throws Exception{
            String id_usuario_modificar_arr = ((String)this.listaUsuarios.getSelectionModel().getSelectedItem());

            String[] id_usuario_modificar_str = new String[0];
            id_usuario_modificar_str = id_usuario_modificar_arr.split("-"); 
            
            System.out.println("Usuario a modificar: " + id_usuario_modificar_str[0]);
            
            this.usuario.borrar_usuario(Integer.parseInt(id_usuario_modificar_str[0]));
            String[] usuarios = usuario.getUsuarios();

            List<String> values = Arrays.asList(usuarios);
            this.listaUsuarios.setItems(FXCollections.observableList(values));
    }       
    
    public void handleRegresarMainAction(ActionEvent event) throws Exception{
        Node node = (Node)event.getSource();
        Stage stage=(Stage) node.getScene().getWindow();
        
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/plataformavalidezpredictiva/fxml/Main.fxml"));        
        
        Parent root = (Parent)fxmlLoader.load();          
        Scene scene = new Scene(root); 
        stage.setScene(scene);
               
        stage.show();
        
    }
    
    
    

}
