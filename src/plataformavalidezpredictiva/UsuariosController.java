/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plataformavalidezpredictiva;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author dynelight
 */
public class UsuariosController implements Initializable {

    /**
     * Initializes the controller class.
     */

    // CambiarContrasena.fxml    
    public PasswordField txt_password_1;
    public PasswordField txt_password_2;
    public Label lbl_au_status;
    
    // Sesion actual
    private int id_usuario;
    
   // Base de datos
    public BD bd;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        System.out.println("Inicializar");
        this.bd = new BD();
    }    
    
    public void setUsuario(int id_usuario){
        this.id_usuario = id_usuario;
    
    }
    
    public void handleCambiarContrasenaAction(ActionEvent event){
        BD bd = new BD();
        boolean cambio = false;
        String password_1 = "";        
        String password_2 = "";
        
        password_1 = this.txt_password_1.getText();
        password_2 = this.txt_password_2.getText();
        
        if(password_1.equals("")||(password_2.equals(""))){
             //TODO: Error
        }
        else {
            if(password_1.equals(password_2)){
                cambio = this.bd.cambiar_contrasena(this.id_usuario, password_1);
            }
        }
    } 
        
    public void handleRegresarMain(ActionEvent event) throws Exception{
        Node node = (Node)event.getSource();
        Stage stage=(Stage) node.getScene().getWindow();
        
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/Main.fxml"));        
        
        Parent root = (Parent)fxmlLoader.load();          
        Scene scene = new Scene(root); 
        stage.setScene(scene);
               
        stage.show();
        
    }
    
    
}
