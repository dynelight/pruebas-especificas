/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plataformavalidezpredictiva;

import java.io.IOException;
import java.net.URL;

import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import plataformavalidezpredictiva.bitacora.controllers.BitacoraController;


/**
 *
 * @author Dyne
 */
public class LoginController implements Initializable {

    @FXML
    public TextField txt_login;
    public Label lbl_login;
    public PasswordField txt_password;
    public String foo;
    

    @FXML
    public void handleButtonAction(ActionEvent event) throws Exception {

        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();

        BD bd = new BD();
        System.out.println(this.txt_login.getText());
        System.out.println(this.txt_password.getText());

        int login = bd.login(this.txt_login.getText(), this.txt_password.getText());

        boolean conn = bd.testConn();
        if (conn) {
            if (login > 0) {

                FXMLLoader fxmlLoaderMain = new FXMLLoader(getClass().getResource("fxml/Main.fxml"));

                Parent root = (Parent) fxmlLoaderMain.load();
                
                MainController controller = fxmlLoaderMain.<MainController>getController();
                BitacoraController bitacora = new BitacoraController();
                      
                controller.setUsuario(this.txt_login.getText(), login);
                
                bitacora.insertar_evento(login, "Login");

                Scene scene = new Scene(root);

                stage.setScene(scene);

                stage.show();
            } else {
                this.lbl_login.setText("Usuario y Contraseña no concuerdan");

            }
        } else {
            this.lbl_login.setText("Conexion a Base de Datos falló");
            this.txt_login.setDisable(true);
            this.txt_password.setDisable(true);
            //TODO: El boton de mandar this. .setDisable(true);
        }

        //TODO: Si no, mensaje de error. 
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.lbl_login.setText("");
    }
}
