/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plataformavalidezpredictiva;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import java.util.List;
import java.util.Arrays;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.util.ArrayList;

/**
 * FXML Controller class
 *
 * @author Dyne
 */
public class GeneradorBasesController implements Initializable {

    public ListView listView;
    public BD bd;
    public RadioButton btn_multiple;
    public RadioButton btn_simple;
    public TextField txt_carrera;
    public TextField txt_ano_paa;
    public TextField txt_ano_ucr;
    public TextField txt_carne_a;
    public TextField txt_cursos;
    public CheckBox chk_select_carrera;
    public CheckBox chk_select_ano_ucr;
    public CheckBox chk_select_ano_paa;
    public CheckBox chk_select_carnes;
    public RadioButton rdo_cursos_todos;
    public RadioButton rdo_cursos_lista;
    public RadioButton rdo_cursos_ninguno;
    public RadioButton rdo_carne_sencillo;
    public RadioButton rdo_carne_conjunto;
    public CheckBox chk_estudiante_estado;
    public CheckBox chk_estudiante_identificacion;
    public CheckBox chk_estudiante_apellido1;
    public CheckBox chk_estudiante_apellido2;
    public CheckBox chk_estudiante_nombre;
    public CheckBox chk_estudiante_fechanacimiento;
    public CheckBox chk_estudiante_genero;
    public CheckBox chk_estudiante_provincia;
    public CheckBox chk_estudiante_descripcionprovincia;
    public CheckBox chk_estudiante_promciclodiver;
    public CheckBox chk_estudiante_promedioadmision;
    public CheckBox chk_estudiante_ano_mejor_prom_admision;
    public CheckBox chk_estudiante_mejor_prom_admision;
    public CheckBox chk_estudiante_colegio;
    public CheckBox chk_estudiante_nombrecolegio;
    public CheckBox chk_estudiante_tipocolegio;
    public CheckBox chk_estudiante_desctipocolegio;
    public CheckBox chk_estudiante_tipomodalidad;
    public CheckBox chk_estudiante_desctipomodalidad;
    public CheckBox chk_estudiante_tipohorario;
    public CheckBox chk_estudiante_desctipohorario;
    public CheckBox chk_paa_ano;
    public CheckBox chk_paa_numero_formulario;
    public CheckBox chk_paa_descripcion_area;
    public CheckBox chk_paa_descripcion_subarea;
    public CheckBox chk_paa_total_items_area;
    public CheckBox chk_paa_total_items_subarea;
    public CheckBox chk_paa_total_correctos_area;
    public CheckBox chk_paa_total_incorrectos_area;
    public CheckBox chk_paa_total_correctos_subarea;
    public CheckBox chk_paa_total_incorrectos_subarea;
    public CheckBox chk_paa_items_correctos;
    public CheckBox chk_paa_items_incorrectos;
    public Button btn_generar_csv;
    public Button btn_carnes_buscar;
    public Label lbl_status;
    public TextField txt_conjunto_carnes_ruta;
    
    public ArrayList<String> carnes_iniciales;
    public ArrayList<String> carnes_finales;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        BD bd = new BD();

        this.txt_conjunto_carnes_ruta.setDisable(true);


        this.lbl_status.setText("");

        //Elegir por defecto opción Multiple
        this.btn_multiple.setSelected(true);

        this.chk_select_ano_paa.setDisable(false);
        this.chk_select_ano_ucr.setDisable(false);
        this.chk_select_carrera.setDisable(false);

        this.rdo_carne_conjunto.setDisable(true);
        this.rdo_carne_sencillo.setDisable(true);

        this.txt_ano_ucr.setDisable(false);
        this.txt_ano_paa.setDisable(false);
        this.txt_carne_a.setDisable(false);

        this.txt_carrera.setDisable(true);
        this.listView.setDisable(true);
        this.txt_ano_paa.setDisable(true);
        this.txt_ano_ucr.setDisable(true);
        this.txt_carne_a.setDisable(true);

        this.rdo_cursos_ninguno.setSelected(true);
        
        this.btn_carnes_buscar.setDisable(true);
        
        /** Quitar temporalmente **/
        this.chk_paa_total_items_area.setVisible(false);
        this.chk_paa_total_items_subarea.setVisible(false);        
        this.chk_paa_total_correctos_subarea.setVisible(false);
        this.chk_paa_total_correctos_area.setVisible(false);
        this.chk_paa_total_incorrectos_area.setVisible(false);
        this.chk_paa_total_incorrectos_subarea.setVisible(false);


    }

    public void handleRadioAction(ActionEvent event) throws Exception {
        if (!this.btn_multiple.isSelected()) {
            this.chk_select_ano_paa.setDisable(true);
            this.chk_select_ano_ucr.setDisable(true);
            this.chk_select_carrera.setDisable(true);
            this.rdo_carne_sencillo.setDisable(false);
            this.rdo_carne_conjunto.setDisable(false);
            this.txt_carne_a.setDisable(true);
            this.txt_ano_paa.setDisable(true);
            this.txt_carne_a.setDisable(true);
            this.rdo_carne_conjunto.setSelected(true);            
            this.txt_ano_ucr.setDisable(true);
            if(this.rdo_carne_conjunto.isSelected()){
                this.btn_carnes_buscar.setDisable(false);
            }
            else {
              this.btn_carnes_buscar.setDisable(true);
             }

        } else {
            this.chk_select_ano_paa.setDisable(false);
            this.chk_select_ano_ucr.setDisable(false);
            this.chk_select_carrera.setDisable(false);
            this.rdo_carne_sencillo.setDisable(true);
            this.rdo_carne_conjunto.setDisable(true);
            this.txt_carne_a.setDisable(true);
            this.rdo_carne_conjunto.setSelected(false);
            this.rdo_carne_sencillo.setSelected(false);
            this.btn_carnes_buscar.setDisable(true);     
        }
    }

    public void handleRadioCarneAction(ActionEvent event) throws Exception {
        if (this.rdo_carne_sencillo.isSelected()) {
            this.txt_carne_a.setDisable(false);
        } else {
            this.txt_carne_a.setDisable(true);
        }
    }

    public void handleCriterioCarrera(ActionEvent event) {
        BD bd = new BD();
        String criterio = this.txt_carrera.getText();

        System.out.println("Criterio " + criterio);

        String[] carreras = bd.getCarreras(criterio);

        List<String> values = Arrays.asList(carreras);
        listView.setItems(FXCollections.observableList(values));
    }

    public void handleSocioSeleccionarTodo(ActionEvent event) {
        this.chk_estudiante_estado.setSelected(true);
        this.chk_estudiante_identificacion.setSelected(true);
        this.chk_estudiante_apellido1.setSelected(true);
        this.chk_estudiante_apellido2.setSelected(true);
        this.chk_estudiante_nombre.setSelected(true);
        this.chk_estudiante_fechanacimiento.setSelected(true);
        this.chk_estudiante_genero.setSelected(true);
        this.chk_estudiante_provincia.setSelected(true);
        this.chk_estudiante_descripcionprovincia.setSelected(true);
    }

    public void handleSocioSeleccionarNada(ActionEvent event) {
        this.chk_estudiante_estado.setSelected(false);
        this.chk_estudiante_identificacion.setSelected(false);
        this.chk_estudiante_apellido1.setSelected(false);
        this.chk_estudiante_apellido2.setSelected(false);
        this.chk_estudiante_nombre.setSelected(false);
        this.chk_estudiante_fechanacimiento.setSelected(false);
        this.chk_estudiante_genero.setSelected(false);
        this.chk_estudiante_provincia.setSelected(false);
        this.chk_estudiante_descripcionprovincia.setSelected(false);
    }

    public void handleColegioSeleccionarTodo(ActionEvent event) {

        this.chk_estudiante_promciclodiver.setSelected(true);
        this.chk_estudiante_promedioadmision.setSelected(true);
        this.chk_estudiante_ano_mejor_prom_admision.setSelected(true);
        this.chk_estudiante_mejor_prom_admision.setSelected(true);
        this.chk_estudiante_colegio.setSelected(true);
        this.chk_estudiante_nombrecolegio.setSelected(true);
        this.chk_estudiante_tipocolegio.setSelected(true);
        this.chk_estudiante_desctipocolegio.setSelected(true);
        this.chk_estudiante_tipomodalidad.setSelected(true);
        this.chk_estudiante_desctipomodalidad.setSelected(true);
        this.chk_estudiante_tipohorario.setSelected(true);
        this.chk_estudiante_desctipohorario.setSelected(true);

    }

    public void handleColegioSeleccionarNada(ActionEvent event) {

        this.chk_estudiante_promciclodiver.setSelected(false);
        this.chk_estudiante_promedioadmision.setSelected(false);
        this.chk_estudiante_ano_mejor_prom_admision.setSelected(false);
        this.chk_estudiante_mejor_prom_admision.setSelected(false);
        this.chk_estudiante_colegio.setSelected(false);
        this.chk_estudiante_nombrecolegio.setSelected(false);
        this.chk_estudiante_tipocolegio.setSelected(false);
        this.chk_estudiante_desctipocolegio.setSelected(false);
        this.chk_estudiante_tipomodalidad.setSelected(false);
        this.chk_estudiante_desctipomodalidad.setSelected(false);
        this.chk_estudiante_tipohorario.setSelected(false);
        this.chk_estudiante_desctipohorario.setSelected(false);

    }

    public void handlePAASeleccionarTodo(ActionEvent event) {
        this.chk_paa_ano.setSelected(true);
        this.chk_paa_numero_formulario.setSelected(true);
        this.chk_paa_descripcion_area.setSelected(true);
        this.chk_paa_descripcion_subarea.setSelected(true);
        this.chk_paa_total_items_area.setSelected(false);
        this.chk_paa_total_items_subarea.setSelected(false);
        this.chk_paa_total_correctos_area.setSelected(false);
        this.chk_paa_total_incorrectos_area.setSelected(false);
        this.chk_paa_total_correctos_subarea.setSelected(false);
        this.chk_paa_total_incorrectos_subarea.setSelected(false);
        this.chk_paa_items_correctos.setSelected(true);
        this.chk_paa_items_incorrectos.setSelected(true);
    }

    public void handlePAASeleccionarNada(ActionEvent event) {
        this.chk_paa_ano.setSelected(false);
        this.chk_paa_numero_formulario.setSelected(false);
        this.chk_paa_descripcion_area.setSelected(false);
        this.chk_paa_descripcion_subarea.setSelected(false);
        this.chk_paa_total_items_area.setSelected(false);
        this.chk_paa_total_items_subarea.setSelected(false);
        this.chk_paa_total_correctos_area.setSelected(false);
        this.chk_paa_total_incorrectos_area.setSelected(false);
        this.chk_paa_total_correctos_subarea.setSelected(false);
        this.chk_paa_total_incorrectos_subarea.setSelected(false);
        this.chk_paa_items_correctos.setSelected(false);
        this.chk_paa_items_incorrectos.setSelected(false);
    }

    public void handleEscogerArchivoCarnesAction(ActionEvent event) {
        FileChooser fc = new FileChooser();
        File file = fc.showOpenDialog(this.btn_generar_csv.getScene().getWindow());
        if (file != null) {
            String filename = file.getAbsolutePath();
            this.txt_conjunto_carnes_ruta.setText(filename);
        }
    }

    public void handleChkCriterioAction(ActionEvent event) {
        if (this.chk_select_carrera.isSelected()) {
            this.txt_carrera.setDisable(false);
            this.listView.setDisable(false);
        } else {
            this.txt_carrera.setDisable(true);
            this.listView.setDisable(true);
        }

        if (this.chk_select_ano_ucr.isSelected()) {
            this.txt_ano_ucr.setDisable(false);
        } else {
            this.txt_ano_ucr.setDisable(true);
        }

        if (this.chk_select_ano_paa.isSelected()) {
            this.txt_ano_paa.setDisable(false);
        } else {
            this.txt_ano_paa.setDisable(true);
        }
    }

    public void handleChkCriterioPAAAction(ActionEvent event) {

        if (this.chk_select_ano_paa.isSelected()) {
            this.txt_ano_paa.setDisable(false);
        } else {
            this.txt_ano_paa.setDisable(true);
        }
    }

    public void handleGenerarCSV(ActionEvent event) {

        BD bd = new BD();


        //Determinan si uso las tablas de PAA y Notas
        Boolean usarTablaNotas = false;
        Boolean usarTablaPAA = false;
        Boolean usarTablaEstudiantesCarreras = false; 

        String log = "";

        //TODO: Sacar el ID
        System.out.println((String) listView.getSelectionModel().getSelectedItem());
        String lv_salida = ((String) listView.getSelectionModel().getSelectedItem());
        String[] id_carrera = new String[0];
        if (this.btn_multiple.isSelected() && this.chk_select_carrera.isSelected()){
            id_carrera = lv_salida.split("-");
        }


        if (this.rdo_cursos_lista.isSelected() || this.rdo_cursos_todos.isSelected()) {
            usarTablaNotas = true;
        }
        String[] params = new String[5];

        this.btn_generar_csv.setDisable(true);

        if (this.btn_multiple.isSelected()) {

            if (this.chk_select_carrera.isSelected()) {
                usarTablaEstudiantesCarreras = true;
//                params[0] = listView.getId().toString(); 
                params[0] = id_carrera[0];
            }
            if (this.chk_select_ano_ucr.isSelected()) {
                params[1] = this.txt_ano_ucr.getText();
            }
            if (this.chk_select_ano_paa.isSelected()) {
                params[3] = this.txt_ano_paa.getText();
                usarTablaPAA = true;    
            }

        } else {
            if (this.rdo_carne_conjunto.isSelected()) {
                params[2] = this.txt_conjunto_carnes_ruta.getText();
            } else {
                params[4] = this.txt_carne_a.getText().toUpperCase();

            }
        }

        String filtro = bd.determinarCriterio(params);
        this.carnes_iniciales = null;
        this.carnes_iniciales = bd.get_carnes_iniciales(filtro);

        /**
         * Ruta a salvar el archivo *
         */
        //String wd = System.getProperty("user.dir");
        FileChooser fc = new FileChooser();
        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fc.getExtensionFilters().add(extFilter);
        this.lbl_status.setText("Cargando ...");

        // Boolean agregarNotas = false;
        //  if(!(this.txt_cursos.getText().isEmpty())){
        //      agregarNotas = true;
        //  }
        //int rc = fc.showDialog(null, "Archivo Destino");



        File file = fc.showSaveDialog(this.btn_generar_csv.getScene().getWindow());
//        if (rc == JFileChooser.APPROVE_OPTION){
        if (file != null) {
//            File file = fc.getSelectedFile();

            String filename = file.getAbsolutePath();

            String selectPAA = "SELECT V_IIP_INFORMACION_ESTUDIANTE.carne, V_IIP_ESTADS_RESULT_PAA_HIS.codigo_area, V_IIP_ESTADS_RESULT_PAA_HIS.codigo_sub_area, ";

            /**
             * Obtiene valores de los checkboxes, para construir SQL Dinámico *
             */
            /**
             * @TODO Hacer esto más elegante *
             */
            String select = "SELECT DISTINCT V_IIP_INFORMACION_ESTUDIANTE.carne, ";
            if (this.chk_estudiante_identificacion.isSelected()) {
                select += "V_IIP_INFORMACION_ESTUDIANTE.identificacion, ";
            }
            if (this.chk_estudiante_estado.isSelected()) {
                select += "V_IIP_INFORMACION_ESTUDIANTE.estado_estudiante, ";
            }
            if (this.chk_estudiante_apellido1.isSelected()) {
                select += "V_IIP_INFORMACION_ESTUDIANTE.apellido1, ";
            }
            if (this.chk_estudiante_apellido2.isSelected()) {
                select += "V_IIP_INFORMACION_ESTUDIANTE.apellido2, ";
            }
            if (this.chk_estudiante_nombre.isSelected()) {
                select += "V_IIP_INFORMACION_ESTUDIANTE.nombre, ";
            }
            if (this.chk_estudiante_fechanacimiento.isSelected()) {
                select += "V_IIP_INFORMACION_ESTUDIANTE.fecha_nacimiento, ";
            }
            if (this.chk_estudiante_genero.isSelected()) {
                select += "V_IIP_INFORMACION_ESTUDIANTE.genero, ";
            }
            if (this.chk_estudiante_provincia.isSelected()) {
                select += "V_IIP_INFORMACION_ESTUDIANTE.provincia, ";
            }
            if (this.chk_estudiante_descripcionprovincia.isSelected()) {
                select += "V_IIP_INFORMACION_ESTUDIANTE.descripcion_provincia, ";
            }
            if (this.chk_estudiante_colegio.isSelected()) {
                select += "V_IIP_INFORMACION_ESTUDIANTE.colegio, ";
            }
            if (this.chk_estudiante_nombrecolegio.isSelected()) {
                select += "V_IIP_INFORMACION_ESTUDIANTE.nombre_colegio, ";
            }
            if (this.chk_estudiante_tipocolegio.isSelected()) {
                select += "V_IIP_INFORMACION_ESTUDIANTE.tipo_colegio, ";
            }
            if (this.chk_estudiante_desctipocolegio.isSelected()) {
                select += "V_IIP_INFORMACION_ESTUDIANTE.descripcion_tipo_colegio, ";
            }
            if (this.chk_estudiante_tipomodalidad.isSelected()) {
                select += "V_IIP_INFORMACION_ESTUDIANTE.tipo_modalidad, ";
            }
            if (this.chk_estudiante_desctipomodalidad.isSelected()) {
                select += "V_IIP_INFORMACION_ESTUDIANTE.descripcion_modalidad_colegio, ";
            }
            if (this.chk_estudiante_tipohorario.isSelected()) {
                select += "V_IIP_INFORMACION_ESTUDIANTE.tipo_horario, ";
            }
            if (this.chk_estudiante_desctipohorario.isSelected()) {
                select += "V_IIP_INFORMACION_ESTUDIANTE.descripcion_horario_colegio, ";
            }
            if (this.chk_estudiante_promciclodiver.isSelected()) {
                select += "V_IIP_INFORMACION_ESTUDIANTE.promedio_ciclo_diver, ";
            }
            if (this.chk_estudiante_promedioadmision.isSelected()) {
                select += "V_IIP_INFORMACION_ESTUDIANTE.promedio_admision, ";
            }
            if (this.chk_estudiante_ano_mejor_prom_admision.isSelected()) {
                select += "V_IIP_INFORMACION_ESTUDIANTE.anno_mejor_prom_admision, ";
            }
            if (this.chk_estudiante_mejor_prom_admision.isSelected()) {
                select += "V_IIP_INFORMACION_ESTUDIANTE.mejor_prom_admision, ";
            }

            if (this.chk_paa_ano.isSelected()) {
                select += "V_IIP_ESTADS_RESULT_PAA_HIS.anno_proceso, ";
            }
            if (this.chk_paa_numero_formulario.isSelected()) {
                select += "V_IIP_ESTADS_RESULT_PAA_HIS.numero_formulario_estudiante, ";
            }
            if (this.chk_paa_descripcion_area.isSelected()) {
                selectPAA += "V_IIP_ESTADS_RESULT_PAA_HIS.descripcion_area, ";
                usarTablaPAA = true;
            }
            if (this.chk_paa_descripcion_subarea.isSelected()) {
                selectPAA += "V_IIP_ESTADS_RESULT_PAA_HIS.descripcion_sub_area, ";
                usarTablaPAA = true;
            }
            if (this.chk_paa_total_items_area.isSelected()) {
                selectPAA += "V_IIP_ESTADS_RESULT_PAA_HIS.total_items_area, ";
                usarTablaPAA = true;
            }
            if (this.chk_paa_total_items_subarea.isSelected()) {
                selectPAA += "V_IIP_ESTADS_RESULT_PAA_HIS.total_items_subarea, ";
                usarTablaPAA = true;
            }
            if (this.chk_paa_total_correctos_area.isSelected()) {
                selectPAA += "V_IIP_ESTADS_RESULT_PAA_HIS.total_correctos_area, ";
                usarTablaPAA = true;
            }
            if (this.chk_paa_total_incorrectos_subarea.isSelected()) {
                selectPAA += "V_IIP_ESTADS_RESULT_PAA_HIS.total_incorrectos_subarea, ";
                usarTablaPAA = true;
            }
            if (this.chk_paa_total_correctos_subarea.isSelected()) {
                selectPAA += "V_IIP_ESTADS_RESULT_PAA_HIS.total_correctos_subarea, ";
                usarTablaPAA = true;
            }
            if (this.chk_paa_items_correctos.isSelected()) {
                selectPAA += "V_IIP_ESTADS_RESULT_PAA_HIS.items_correctos, ";
                usarTablaPAA = true;
            }
            if (this.chk_paa_items_incorrectos.isSelected()) {
                selectPAA += "V_IIP_ESTADS_RESULT_PAA_HIS.items_incorrectos, ";
                usarTablaPAA = true;
            }

            select = select.substring(0, select.length() - 2); //Quitar coma y espacio final
            selectPAA = selectPAA.substring(0, selectPAA.length() - 2); //Quitar coma y espacio final

            select += " FROM V_IIP_INFORMACION_ESTUDIANTE";
            
            if(usarTablaEstudiantesCarreras){
                select += ", V_IIP_CARRERA_ESTUDIANTE";
            }
            if (usarTablaPAA) {
                select += ", V_IIP_ESTADS_RESULT_PAA_HIS";  //Ligaduras
            }

            if (usarTablaNotas) {
                select += ", V_IIP_NOTAS_ESTUDIANTE";  //Ligaduras
            }

            select += " WHERE 1=1 ";
            
            if(usarTablaEstudiantesCarreras){
                select += " AND V_IIP_INFORMACION_ESTUDIANTE.carne = V_IIP_CARRERA_ESTUDIANTE.carne ";
            
            }

            if (usarTablaPAA) {
                select += " AND V_IIP_ESTADS_RESULT_PAA_HIS.identificacion_estudiante = V_IIP_INFORMACION_ESTUDIANTE.identificacion ";

            }

            if (usarTablaNotas) {
                select += " AND V_IIP_NOTAS_ESTUDIANTE.carne = V_IIP_INFORMACION_ESTUDIANTE.carne ";
            }
            select += filtro;


            // if(agregarNotas){
            String[] cursos = this.txt_cursos.getText().split(",");


            if (this.txt_cursos.equals("")) {
                select += "AND (";
                for (int i = 0; i < cursos.length; i++) {
                    select += "V_IIP_NOTAS_ESTUDIANTE.sigla = '" + cursos[i] + "'";
                    if (i != cursos.length - 1) {
                        select += " OR ";
                    }
                }
                select += ")";
            }
            if (usarTablaNotas) {
                select += " AND V_IIP_INFORMACION_ESTUDIANTE.carne = V_IIP_NOTAS_ESTUDIANTE.carne";
            }
            // }



            System.out.println("Antes func: " + select);

            /**
             * Ejecuta query, convierte a CSV y salva en el archivo *
             */
            String csv = bd.getCSV(select, selectPAA, cursos, filtro, cursos, usarTablaNotas, usarTablaPAA, usarTablaEstudiantesCarreras);
            try {
                BufferedWriter out = new BufferedWriter(new FileWriter(filename));
                out.write(csv);
                out.close();
                
                this.lbl_status.setText("Generado con éxito!");
                                
                System.out.println("Carnes originales: ");
                System.out.print("[ ");
                for (int i = 0; i < this.carnes_iniciales.size(); i++) {
                    System.out.print( this.carnes_iniciales.get(i) );
                    System.out.print(" ");                    
                }
                System.out.println("]");

                this.carnes_finales = bd.get_carnes_finales();

                System.out.println("Carnes finales: ");
                
                System.out.print("[ ");
                for (int i = 0; i < this.carnes_finales.size(); i++) {
                    System.out.print( this.carnes_finales.get(i) );
                    System.out.print(" ");                    
                }
                System.out.print("]");
                System.out.println("");     
                System.out.println("LISTO!");

                //bitacora();

            } catch (Exception e) {
            }
        } else {
            System.out.println("Acción Cancelada");
        }
        this.btn_generar_csv.setDisable(false);

        //  }
    }

    public void bitacora() {
        String log = "";


        log += "Sociodemograficas: { ";
        if (this.chk_estudiante_identificacion.isSelected()) {
            log += "identificacion ";
        }
        if (this.chk_estudiante_estado.isSelected()) {
            log += "estado  ";
        }
        if (this.chk_estudiante_apellido1.isSelected()) {
            log += "apellido1 ";
        }
        if (this.chk_estudiante_apellido2.isSelected()) {
            log += "apellido2 ";
        }
        if (this.chk_estudiante_nombre.isSelected()) {
            log += "nombre ";
        }
        if (this.chk_estudiante_fechanacimiento.isSelected()) {
            log += "fecha_nacimiento ";
        }
        if (this.chk_estudiante_genero.isSelected()) {
            log += "genero ";
        }
        if (this.chk_estudiante_provincia.isSelected()) {
            log += "provincia ";
        }
        if (this.chk_estudiante_descripcionprovincia.isSelected()) {
            log += "descripcion_provincia ";
        }

        log += "} \n Colegio: { ";
        if (this.chk_estudiante_colegio.isSelected()) {
            log += "colegio ";
        }
        if (this.chk_estudiante_nombrecolegio.isSelected()) {
            log += "nombre_colegio ";
        }
        if (this.chk_estudiante_tipocolegio.isSelected()) {
            log += "tipo_colegio ";
        }
        if (this.chk_estudiante_desctipocolegio.isSelected()) {
            log += "descripcion_tipo_colegio ";
        }
        if (this.chk_estudiante_tipomodalidad.isSelected()) {
            log += "tipo_modalidad ";
        }
        if (this.chk_estudiante_desctipomodalidad.isSelected()) {
            log += "dscripcion_tipo_modalidad ";
        }
        if (this.chk_estudiante_tipohorario.isSelected()) {
            log += "tipo_horario ";
        }
        if (this.chk_estudiante_desctipohorario.isSelected()) {
            log += "descripcion_tipo_horario ";
        }
        if (this.chk_estudiante_promciclodiver.isSelected()) {
            log += "promedio_ciclo_diversificado ";
        }
        if (this.chk_estudiante_promedioadmision.isSelected()) {
            log += "promedio_admision ";
        }
        if (this.chk_estudiante_ano_mejor_prom_admision.isSelected()) {
            log += "ano_mejor_promedio_admision ";
        }
        if (this.chk_estudiante_mejor_prom_admision.isSelected()) {
            log += "mejor_promedio_admision ";
        }

        log += "} \n PAA {";

        if (this.chk_paa_ano.isSelected()) {
            log += "año_processo ";
        }
        if (this.chk_paa_numero_formulario.isSelected()) {
            log += "numero_formulario ";
        }
        if (this.chk_paa_descripcion_area.isSelected()) {
            log += "descripcion_area ";
        }
        if (this.chk_paa_descripcion_subarea.isSelected()) {
            log += "descripcion_subarea ";
        }
        if (this.chk_paa_total_items_area.isSelected()) {
            log += "total_items_area ";
        }
        if (this.chk_paa_total_items_subarea.isSelected()) {
            log += "total_items_subarea ";
        }
        if (this.chk_paa_total_correctos_area.isSelected()) {
            log += "total_correctos_area ";
        }
        if (this.chk_paa_total_incorrectos_subarea.isSelected()) {
            log += "total_incorrectos_subarea ";
        }
        if (this.chk_paa_total_correctos_subarea.isSelected()) {
            log += "total_correctos_subarea ";
        }
        if (this.chk_paa_items_correctos.isSelected()) {
            log += "items_correctos ";
        }
        if (this.chk_paa_items_incorrectos.isSelected()) {
            log += "incorrectos ";
        }

        log += "}";
        System.out.println(log);


    }
    
    public void handleRegresarMainAction(ActionEvent event) throws Exception{
        Node node = (Node)event.getSource();
        Stage stage=(Stage) node.getScene().getWindow();
        
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/Main.fxml"));        
        
        Parent root = (Parent)fxmlLoader.load();          
        Scene scene = new Scene(root); 
        stage.setScene(scene);
               
        stage.show();
        
    }
    
}           
//}
