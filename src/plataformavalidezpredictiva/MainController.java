/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plataformavalidezpredictiva;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
/**
 * FXML Controller class
 *
 * @author dynelight
 */
public class MainController implements Initializable {

    /**
     * Initializes the controller class.
     */
    public MenuBar barra_menu;
    public Label lbl_usuario;
    public Menu menu_bar;
    public int id_usuario;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        

    }    
    
    public void handleCargarGeneradorBases(ActionEvent event) throws Exception{

        Stage stage = (Stage) this.barra_menu.getScene().getWindow();        
        
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/GeneradorBases.fxml"));        
        
        Parent root = (Parent)fxmlLoader.load();          
        Scene scene = new Scene(root); 
        stage.setScene(scene);
               
        stage.show();
        //TODO: Si no, mensaje de error. 
    }

    public void handleCargarGestionUsuarios(ActionEvent event) throws Exception{

        Stage stage = (Stage) this.barra_menu.getScene().getWindow();
        
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("usuarios/fxml/GestionUsuarios.fxml"));        
        
        Parent root = (Parent)fxmlLoader.load();          
        Scene scene = new Scene(root); 
        stage.setScene(scene);
               
        stage.show();
        //TODO: Si no, mensaje de error. 
    }

        public void handleCargarGestionBitacora(ActionEvent event) throws Exception{

        Stage stage = (Stage) this.barra_menu.getScene().getWindow();
        
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("bitacora/fxml/GestionBitacora.fxml"));        
        
        Parent root = (Parent)fxmlLoader.load();          
        Scene scene = new Scene(root); 
        stage.setScene(scene);
               
        stage.show();
        //TODO: Si no, mensaje de error. 
    }

    public void setUsuario(String usuario, int id_usuario){
        this.id_usuario = id_usuario;
        this.lbl_usuario.setText(usuario);
    
    }
    
    public void handleCargarCambioContrasena(ActionEvent event) throws Exception{
        //Node node = (Node)event.getSource();
        //Stage stage=(Stage) node.getScene().getWindow();
        Stage stage = (Stage) this.barra_menu.getScene().getWindow();        
        //Scene scene = Main.screens.get("tweet");
        
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("usuarios/fxml/CambiarContrasena.fxml"));        
        
        Parent root = (Parent)fxmlLoader.load();   
        
        UsuariosController controller = fxmlLoader.<UsuariosController>getController();
        controller.setUsuario(this.id_usuario);
                
        Scene scene = new Scene(root); 
        stage.setScene(scene);
               
        stage.show();
    }    
    
    public void handleCerrarSesion(ActionEvent event) throws Exception{
        //Node node = (Node)event.getSource();
        //Stage stage=(Stage) node.getScene().getWindow();
        Stage stage = (Stage) this.barra_menu.getScene().getWindow() ;        
        //Scene scene = Main.screens.get("tweet");
        
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/Login.fxml"));        
        
        Parent root = (Parent)fxmlLoader.load();   
        
        //LoginController controller = fxmlLoader.<LoginController>getController();
                
        Scene scene = new Scene(root); 
        stage.setScene(scene);
               
        stage.show();
    }     
    
        public void handleSalir(ActionEvent event) throws Exception{
            Stage stage = (Stage) this.barra_menu.getScene().getWindow(); 
            stage.close();
    }
    
}
