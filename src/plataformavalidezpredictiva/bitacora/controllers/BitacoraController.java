/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plataformavalidezpredictiva.bitacora.controllers;

import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import plataformavalidezpredictiva.clases.Bitacora;
/**
 *
 * @author dynelight
 */
public class BitacoraController implements Initializable {
    
    public Bitacora bitacora;
    public ListView listaBitacora;    
    
    public BitacoraController(){
        this.bitacora = new Bitacora();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.bitacora = new Bitacora();       
        String[] bitacoras = bitacora.getBitacora();

        List<String> values = Arrays.asList(bitacoras);
        
        this.listaBitacora.setItems(FXCollections.observableList(values));        

    }  
    
    public void insertar_evento(int id_usuario, String evento){
        bitacora.insertar_evento(id_usuario, evento);
    }
 
    public void handleRegresarMainAction(ActionEvent event) throws Exception{
        Node node = (Node)event.getSource();
        Stage stage=(Stage) node.getScene().getWindow();
        
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/plataformavalidezpredictiva/fxml/Main.fxml"));        
        
        Parent root = (Parent)fxmlLoader.load();          
        Scene scene = new Scene(root); 
        stage.setScene(scene);
               
        stage.show();
        
    }

    
}
